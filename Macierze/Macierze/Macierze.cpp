// Macierze.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <omp.h>
#include <cstdio>
#include <assert.h>
#include <conio.h>
#include <iostream>

using namespace std;

#define ONE (1)
#define MX (1000)
#define NOMINAL (1000)

// Tablice dwuwymiarowe - macierze
int a[MX][MX];
int b[MX][MX];
int c[MX][MX];
int d[MX][MX];
int g[MX][MX];

// Tablice dwuwymiarowe z jedna kolumna - wektory
int e[ONE][MX]; //wektor, taki zapis ułatwia liczenie
int f[MX][MX];
int h[MX][MX];
int k[MX][MX];

void generate_matrix(int n)
{
	int i;
	int j;
	for (i = 0; i<n; i++)
	{
		for (j = 0; j<n; j++)
		{
			int v = (i * 2 + j) % 100;
			a[i][j] = v;
			b[i][j] = v;
		}
	}
}

void generate_wektor(int n)
{
	int i;
	for (i = 0; i<n; i++)
	{
		int v = (i * 2 + 1) % 100;
		//tworzenie odpowiedniego wektora
		e[0][i] = v;
	}
}

void matrix_mult_serial(int n)
{
	int i;
	int j;
	int k;
	double st = omp_get_wtime();

	memset(c, 0, sizeof(c)); //zerowanie tablicy za pomoca funkcji bibliotecznej

	for (i = 0; i<n; i++)
	{
		for (j = 0; j<n; j++)
		{
			for (k = 0; k<n; k++)
			{
				c[i][j] += a[i][k] * b[k][j];
			}
		}
	}
	double en = omp_get_wtime();
	printf("Macierz - (zwyczajnie) \t- czas %lf\n", en - st);
}

void matrix_mult_parallel1(int n)
{
	int i;
	int j;
	int k;
	double st = 0.0;
	double en = 0.0;

	memset(d, 0, sizeof(d)); //zerowanie tablicy za pomoca funkcji bibliotecznej

	st = omp_get_wtime();
#pragma omp parallel for schedule(static,50) private(i,j,k) shared(a,b,d)
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < n; j++)
		{
			for (k = 0; k < n; k++)
			{
				d[i][j] += a[i][k] * b[k][j];
			}
		}
	}
	en = omp_get_wtime();
	printf("Macierz - (Statyczne) \t- czas %lf\n", en - st);
}
void matrix_mult_parallel2(int n)
{
	//Dynamic Scheduler
	int i;
	int j;
	int k;
	double st = 0.0;
	double en = 0.0;

	memset(g, 0, sizeof(g)); //zerowanie tablicy za pomoca funkcji bibliotecznej

	st = omp_get_wtime();
#pragma omp parallel for schedule(dynamic) private(i,j,k) shared(a,b,g)
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < n; j++)
		{
			for (k = 0; k < n; k++)
			{
				g[i][j] += a[i][k] * b[k][j];
			}
		}
	}
	en = omp_get_wtime();
	printf("Macierz - (Dynamiczne) \t- czas %lf\n", en - st);
}

void wektor(int n)
{
	int i;
	int j;
	double st = 0.0;
	double en = 0.0;

	memset(f, 0, sizeof(f)); //zerowanie tablicy za pomoca funkcji bibliotecznej

	st = omp_get_wtime();
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < n; j++)
		{
			f[0][i] += a[i][j] * e[0][j];
		}
	}
	en = omp_get_wtime();
	printf("Wektor - (zwyczajnie) \t- czas %lf\n", en - st);
}

void wektor_parrallel(int n)
{
	//Dynamic Scheduler
	int i;
	int j;
	double st = 0.0;
	double en = 0.0;

	memset(k, 0, sizeof(k)); //zerowanie tablicy za pomoca funkcji bibliotecznej

	st = omp_get_wtime();
#pragma omp parallel for schedule(static,50) private(i,j) shared(a,k,e)
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < n; j++)
		{
			k[0][i] += a[i][j] * e[0][j];
		}
	}
	en = omp_get_wtime();
	printf("Wektor - (Statyczne) \t- czas %lf\n", en - st);
}

void wektor_parrallel2(int n)
{
	//Dynamic Scheduler
	int i;
	int j;
	double st = 0.0;
	double en = 0.0;

	memset(h, 0, sizeof(h)); //zerowanie tablicy za pomoca funkcji bibliotecznej

	st = omp_get_wtime();
#pragma omp parallel for schedule(dynamic) private(i,j) shared(a,h,e)
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < n; j++)
		{
			h[0][i] += a[i][j] * e[0][j];
		}
	}
	en = omp_get_wtime();
	printf("Wektor - (Dynamiczne) \t- czas %lf\n", en - st);
}

bool porownajMacierze(int a[MX][MX], int b[MX][MX])
{
	for (int i = 0; i < MX; i++)
	{
		for (int j = 0; j < MX; j++)
		{
			if (a[i][j] != b[i][j])
				return false;
		}
	}
	return true;
}

int main(int argc, char *argv[])
{
	
	setlocale(LC_ALL, "");
	
	if (NOMINAL > MX)
	{
		cout << "Wartość nominalna nie może być większa od maksymalnej!" << endl;
		_getch();
		exit(1);
	}
	// Macierze
	generate_matrix(NOMINAL);

	matrix_mult_serial(NOMINAL);
	matrix_mult_parallel1(NOMINAL);
	matrix_mult_parallel2(NOMINAL);

	cout << "Porównanie macierzy c i d \t" << porownajMacierze(c, d) << endl;
	cout << "Porównanie macierzy c i g \t" << porownajMacierze(c, g) << endl << endl;
	// Wektory
	generate_wektor(NOMINAL);
	
	wektor(NOMINAL);
	wektor_parrallel(NOMINAL);
	wektor_parrallel2(NOMINAL);

	cout << "Porównanie obliczeń liczonych f i h \t" << porownajMacierze(f, h) << endl;
	cout << "Porównanie obliczeń liczonych f i k \t" << porownajMacierze(f, k) << endl;

	_getch();
	return 0;
}