// Calka v2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <conio.h>
#include <iostream>
#include <math.h>
#include <omp.h>
using namespace std;
using namespace std;

#define M_PI           (3.14159265358979323846)  /* pi */

class My {
public:

	void integrate(double a, double b, int ki, int kf)
	{
		omp_set_nested(1);
		
		cout << "Calka I(k) z " << "f(x) = (exp(-2*x)/sqrt(1+pow(x,2)+pow(k,2));)" << endl;
		
		int numOfParts = (int)(b - a) * 30000;
		double dx = (b - a) / (numOfParts);

		double sum = 0; double sumOfSums = 0; int i = 0;

		//#pragma omp parallel for schedule(static) private(i)  reduction(+:sum) reduction(+:sumOfSums) // schedule - static
		#pragma omp parallel for default(shared) private(i,sum) \
		schedule(dynamic)\
		reduction(+:sumOfSums)
		for (int k = kf; k >= ki; k--)
		{
			sum = 0;
			#pragma omp parallel for default(shared) private(i)\
			schedule(dynamic)\
			reduction(+:sum)

			for (i = 0; i<numOfParts; i++)
			{
				sum += fun(a + i*dx + dx / 2, k)*dx;
			}
			#pragma omp critical 
			cout << "dla k = " << k << " jest rowna I(" << k << ") = " << sum << endl;
			sumOfSums += sum;
		}
		cout << "sumOfSums = " << sumOfSums << endl;
	}

	double fun(double x, int k)
	{
		return exp(-2*x)/sqrt(1 + pow(x, 2) + pow(k, 2));
	}

};

int main() {
	My m;
	double st = omp_get_wtime();
	m.integrate(0, 5*M_PI, 1, 100);
	double en = omp_get_wtime();
	cout << "Czas: " << en - st;
	_getch();
}

