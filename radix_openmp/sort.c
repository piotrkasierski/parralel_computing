#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <omp.h>

#define TABLE_SIZE (100000000)
#define MAX_VALUE (10000)
#define MICROSECONDS_IN_SECOND (1000000)
#define DIGIT_SIZE (10)

void generate_int_number(unsigned long long int *numbers, unsigned long long int array_size, unsigned long long int max_value) {
  unsigned long long int i = 0;

  printf("Generating numbers: array size %d, max value: %d\n", array_size,
         max_value);

  srand(time(NULL));

  for (i = 0; i < array_size; i++) {
    numbers[i] = rand() % max_value;
  }
}

int sort(unsigned long long int *table, unsigned long long int table_size)
{
    long long int i = 0;
    long long int digit = 0;
    unsigned long long int max_val = 0;
    unsigned long long int counted[DIGIT_SIZE] = {0};
    unsigned long long int *temp_table = NULL;
    unsigned long long int counted_0 = 0;
    unsigned long long int counted_1 = 0;
    unsigned long long int counted_2 = 0;
    unsigned long long int counted_3 = 0;
    unsigned long long int counted_4 = 0;
    unsigned long long int counted_5 = 0;
    unsigned long long int counted_6 = 0;
    unsigned long long int counted_7 = 0;
    unsigned long long int counted_8 = 0;
    unsigned long long int counted_9 = 0;

    max_val = table[0];

#pragma omp parallel for shared(table) private(i)
    for (i = 1; i < table_size; i++) {
        if (table[i] > max_val) {
#pragma omp critical
	    {
    		max_val = table[i];
	    }
	}
    }

    if (max_val < 1) {
	printf("Max value error\n");
        return 1;
    }

    temp_table = malloc(table_size * sizeof(unsigned long long int));
    if (temp_table == NULL) {
        printf("Mem alloc error\n");
	return 1;
    }

    for (digit = 1; (max_val / digit) > 0; digit *= DIGIT_SIZE) {

#pragma omp parallel for shared(table) private(i) \
reduction(+:counted_0,counted_1,counted_2,counted_3,counted_4,counted_5,counted_6,counted_7,counted_8,counted_9)
        for (i = 0; i < table_size; i++) {
            switch ((table[i] / digit) % DIGIT_SIZE) {
 	        case 0: 
                    counted_0++;
	            break;
	        case 1: 
                    counted_1++;
		    break;
 	        case 2: 
                    counted_2++;
		    break;
	        case 3: 
                    counted_3++;
		    break;
	        case 4: 
                    counted_4++;
		    break;
	        case 5: 
                    counted_5++;
		    break;
	        case 6: 
                    counted_6++;
		    break;
	        case 7: 
                    counted_7++;
		    break;
	        case 8: 
                    counted_8++;
		    break;
	        case 9: 
                    counted_9++;
		    break;
	    }
	}

	counted[0] = counted_0;
	counted[1] = counted_1 + counted_0;
	counted[2] = counted_2 + counted_1;
	counted[3] = counted_3 + counted_2;
	counted[4] = counted_4 + counted_3;
	counted[5] = counted_5 + counted_4;
	counted[6] = counted_6 + counted_5;
	counted[7] = counted_7 + counted_6;
	counted[8] = counted_8 + counted_7;
	counted[9] = counted_9 + counted_8;

#pragma omp parallel for shared(counted,temp_table) private(i,table)
	for (i = table_size - 1; i == 0; i--)
        {
            temp_table[counted[(table[i] / digit) % DIGIT_SIZE] - 1] = table[i];
#pragma omp critical
	    {
                counted[(table[i] / digit) % DIGIT_SIZE]--;
	    }
        }

#pragma omp parallel for shared(table,temp_table) private(i)
        for (i = 0; i < table_size; i++) {
            table[i] = temp_table[i];
	}
    }

    free(temp_table);
    return 0;
}

int check_sort(unsigned long long int *numbers, unsigned long long int array_size) {
  unsigned long long int i = 0;

  for (i = 0; i < array_size - 1; i++) {
    if (numbers[i] > numbers[i + 1]) {
      printf("Check: Not sorted!\n");
      return 1;
    }
  }
  printf("Check: Sorted\n");

  return 0;
}

int main(int argc, char **argv) {
  int status = 0;
  clock_t start = {0};
  clock_t end = {0};
  unsigned long long int *table  = NULL;
  unsigned long long int table_size = 0;
  unsigned long long int max_value = 0;

  table_size = TABLE_SIZE;
  max_value = MAX_VALUE;

  table = calloc(table_size, sizeof(unsigned long long int));
  generate_int_number(table, table_size, max_value);
  status = check_sort(table, table_size);

  printf("Sort: start\n");
  start = clock();
  sort(table, table_size);
  end = clock();
  printf("Sort: stop\n");

  printf("Time: %.3fs\n", ((float)(end - start) / MICROSECONDS_IN_SECOND));
  status = check_sort(table, table_size);

  free(table);

  return status;
}