// Rownaniekwadratowe.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <conio.h>
#include <math.h>

int main()
{
	float a;
	float b;
	float c;
	float delta;
	float Re1, Re2, Im1, Im2;
	char wyjscie = 'n';
	using namespace std;

	while (wyjscie != 't') {

		cout << "\n\rPodaj liczbe a: ";
		cin >> a;
		cout << "Podaj liczbe b: ";
		cin >> b;
		cout << "Podaj liczbe c: ";
		cin >> c;
		delta = b*b - 4 * a*c;
		cout << "\n\rDelta: " << delta;
		if (delta == 0) {
			cout << "\n\rFunckja ma jedno miejsce zerowe. Jest nim: " << -b / (2 * a);
		}
		else if (delta < 0) {
			cout << endl << "Brak rozwiazan rzeczywistych, ale istnieja rozwiazania urojone" << endl;
			Re1 = Re2 = -b / (2 * a);
			Im1 = fabs(sqrt(fabs(delta)) / (2 * a));
			cout << "x1 - " << Re1 << " + " << Im1 << "i" << endl;
			cout << "x2 - " << Re2 << " - " << Im1 << "i" << endl;
		}
		else if (delta > 0) {
			cout << "\n\rFunckja ma dwa miejsca zerowe: x1 - " << (-b - sqrt(delta)) / (2 * a) << " oraz x2 - " << (-b + sqrt(delta)) / (2 * a);
		}
		cout << "\n\rWyjdz z programu (t=wyjscie, inny klawisz zostan): ";
		wyjscie = _getch();
	}
	return 0;
}