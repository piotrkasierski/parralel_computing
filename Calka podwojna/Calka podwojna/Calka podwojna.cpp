// Calka podwojna.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <cmath>
#include <ctime>
#include <chrono>
#include <omp.h>



using namespace std;

class Podwojna {
public:
	// FUNCKJA CAŁKOWANIA
	double fun(double x, double y)
	{
		return exp(-(pow(x, 2) + pow(y, 2))) / sqrt(1 + pow(x, 2) + pow(y, 2));
	}

	// BEZ ZROWNOLEGLENIA
	void integrate(double a, double b, double ay, double by)
	{
		// Inicjowanie zmiennych
		double sum = 0;
		double sumOfSums = 0;
		int i = 0;
		int j = 0;
		double st = 0.0;
		double en = 0.0;
		int numOfPartsX = (int)((b - a) * 5000);
		double dx = (b - a) / (numOfPartsX); //wyliczenie dx
		int numOfPartsY = (int)((by - ay) * 5000);
		double dy = (by - ay) / (numOfPartsY); //wyliczenie dy

		st = omp_get_wtime(); // Start pomiaru czasu
		for (j = 0; j<numOfPartsY; j++)
		{
			for (i = 0; i < numOfPartsX; i++)
			{
				// znajduje się w środku jednego prostokąta dzięki dzieleniu przez 2
				sum += fun(a + i * dx + dx / 2, ay + j * dy + dy / 2)*dx*dy; 
			}
		}
		en = omp_get_wtime(); // Koniec pomiaru czasu
		cout << "Calka - bez zrownoleglenia \t- wynik = " << sum << "\t - czas obliczeń " << en - st << endl;
	}

	// I SPOSOB
	void integrateParallel(double a, double b, double ay, double by)
	{
		// Inicjowanie zmiennych
		double sum = 0.0; 
		double sumOfSums = 0.0; 
		int i = 0; 
		int j = 0;
		double st = 0.0;
		double en = 0.0;
		int numOfPartsX = (int)((b - a) * 5000);
		double dx = (b - a) / (numOfPartsX); //wyliczenie dx
		int numOfPartsY = (int)((by - ay) * 5000);
		double dy = (by - ay) / (numOfPartsY); //wyliczenie dy

		omp_set_nested(1); 
		st = omp_get_wtime(); // Start pomiaru czasu
#pragma omp parallel for default(shared) private(j,sum) reduction(+:sumOfSums)
		for (j = 0; j < numOfPartsY; j++)
		{
			sum = 0;
#pragma omp parallel for private(i) default(shared) reduction(+:sum) 
			for (i = 0; i < numOfPartsX; i++) {
				// znajduje się w środku jednego prostokąta dzięki dzieleniu przez 2
				sum += fun(a + i * dx + dx / 2, ay + j * dy + dy / 2)*dx*dy;
			}
			sumOfSums += sum;
		}
		en = omp_get_wtime(); // Koniec pomiaru czasu
		cout << "Calka - sposob I \t\t- wynik = " << sumOfSums << "\t - czas obliczeń " << en - st << endl;
	}

    // II SPOSOB
	void integrateParallel2(double a, double b, double ay, double by)
	{
		// Inicjowanie zmiennych
		double sum = 0.0;
		double sumOfSums = 0.0;
		int i = 0;
		int j = 0;
		double st = 0.0;
		double en = 0.0;
		int numOfPartsX = (int)((b - a) * 5000);
		double dx = (b - a) / (numOfPartsX); //wyliczenie dx
		int numOfPartsY = (int)((by - ay) * 5000);
		double dy = (by - ay) / (numOfPartsY); //wyliczenie dy

		omp_set_nested(1);
		st = omp_get_wtime();
#pragma omp parallel for schedule(static) private(j,sum) default(shared) reduction(+:sumOfSums)
		for (j = 0; j < numOfPartsY; j++)
		{
			sum = 0;
#pragma omp parallel for schedule(static) private(i) default(shared) reduction(+:sum)
			for (i = 0; i < numOfPartsX; i++) 
			{
				// znajduje się w środku jednego prostokąta dzięki dzieleniu przez 2
				sum += fun(a + i * dx + dx / 2, ay + j * dy + dy / 2)*dx*dy; //
			}
			sumOfSums += sum;
		}
		en = omp_get_wtime();
		cout << "Calka - sposob II \t\t- wynik = " << sumOfSums << "\t - czas obliczeń " << en - st << endl;
	}
};

int main() {
	setlocale(LC_ALL, "");
	cout << "Calka z " << "f(x)=exp(-(pow(x, 2) + pow(y, 2))) / sqrt(1 + pow(x,2) + pow(y, 2) " << " od ax = 0 do bx = 1 oraz ay= 0 do by= 1" << endl << endl;
	Podwojna calka;
	for (int i = 0; i < 5; i++)
	{
		calka.integrate(0, 1, 0, 1); // Bez zrownoleglenia
		calka.integrateParallel(0, 1, 0, 1); // Sposob I
		calka.integrateParallel2(0, 1, 0, 1); // Sposob II
		cout << endl;
	}
	cout << "Koniec" << endl;
	_gettch();
}