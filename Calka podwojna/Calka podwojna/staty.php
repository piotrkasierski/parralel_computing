function statystyka($tablica) {


//iloć danych
$statystyka[ilo] = count($tablica);


//maksymalna wartoć

if ($statystyka[ilo] == 1) {
$statystyka[max] = $tablica[0];
}

if ($statystyka[ilo] == 0) {
$statystyka[max] = 0;
}

if ($statystyka[ilo] >=  2) {
rsort($tablica);
$statystyka[max] = $tablica[0];
}



//minimalna wartoć

if ($statystyka[ilo] == 1) {
$statystyka[min] = $tablica[0];
}

if ($statystyka[ilo] == 0) {
$statystyka[min] = 0;
}

if ($statystyka[ilo] >=  2) {
sort($tablica);
$statystyka[min] = $tablica[0];
}

//rozpiętoć danych
if ($statystyka[ilo] >=  2) {
$statystyka[roz] = $statystyka[max] - $statystyka[min];
} else {
$statystyka[roz] = 0;
}

//suma danych
if ($statystyka[ilo] >=  2) {
$statystyka[sum] = array_sum($tablica);
} 

if ($statystyka[ilo] == 1) {
$statystyka[sum] = $tablica;
}

if ($statystyka[ilo] == 0) {
$statystyka[sum] = 0;
}


//srednia
if ($statystyka[ilo] == 0) {
$statystyka[srd] = 0;
}
if ($statystyka[ilo] == 1) {
$statystyka[srd] = $tablica[0];
}
if ($statystyka[ilo] >=  2) {
$statystyka[srd] = $statystyka[sum] / $statystyka[ilo];
}


//mediana 

if ($statystyka[ilo] == 0) {
$statystyka[med] = 0;
}

if ($statystyka[ilo] == 1) {
$statystyka[med] = $tablica[0];
}

if ($statystyka[ilo] >=  2) {
if (parzysta($statystyka[ilo])) {

$polowa1 = $statystyka[ilo] / 2;
$polowa2 = $polowa1 - 1;

$indeks1 = $tablica[$polowa1];
$indeks2 = $tablica[$polowa2];

$suma_indeksow = $indeks1 + $indeks2;

$statystyka[med] = $suma_indeksow / 2;

} else {

$polowa = $statystyka[ilo] / 2;

$statystyka[med] = $tablica[$polowa];

}
}

//odchylenie standardowe

if ($statystyka[ilo] == 0) {
$statystyka[odc] = 0;
}

if ($statystyka[ilo] == 1) {
$statystyka[odc] = 0;
}

if ($statystyka[ilo] >=  2) {
$suma_odch = 0;

foreach ($tablica as $liczba_staty) {

$wariacje = $liczba_staty - $statystyka[srd];

$wariacje = pow($wariacje,2);

$suma_odch = $suma_odch + $wariacje;

}

$suma_odch_podzielone = $suma_odch / $statystyka[ilo];

$statystyka[odc] = pow($suma_odch_podzielone,1/2);
}


//wspołczynnik zmiennoci

if ($statystyka[odc] !== 0 && $statystyka[srd] !==0) {
$statystyka[wsp] = ($statystyka[odc] / $statystyka[srd]) * 100;
} else {
$statystyka[wsp] = 0;
}